require 'socket'

host = 'irc.toribash.com'
port = 6667
channel = '#foobar'

nickname = 'foobot'
username = 'foobaaaat'
fullname = 'Just Foobot'

owner = 'eleeleth'
ops = ['Tapion', 'charles', 'Blam', 'Hyde', 'bust3r']
users = []

leave = false
loggedIn = false
restart = false

socket = TCPSocket.new host, port

def setNick(nick)
  return 'NICK ' + nick
end

def login(nick, user, name)
  hostname = Socket.gethostname
  
  # Must prepend a colon to any strings that contain spaces.
  name = ':' + name

  # Construct the strings to send.
  arr = ['USER', nick, '0', '*', name]

  return arr.join(' ')
end 

def join_channel(channel)
  return 'JOIN ' + channel
end

def print_to_socket(sock, msg)
  sock.puts msg
  puts msg
end

def get_users(sock)
  print_to_socket sock, 'NAMES #foobar'
  users = sock.gets.split(':')[-1].split(' ')
  users.map! {|user| user.gsub /[@%&~+]*/, ''}
  return users
end

#define line as nil
line = nil

while leave == false 

  if !loggedIn
    line = socket.gets
    puts line

    print_to_socket socket, setNick(nickname) 
    print_to_socket socket, login(nickname, username, fullname) 
    print_to_socket socket, join_channel(channel)

    loggedIn = true
  end 

  line = socket.gets
  puts line

  if line and line.start_with? 'PING'
    pong = 'PONG ' + line.split(' ')[1]
    print_to_socket socket, pong
  end

  if /PRIVMSG/ =~ line

    user = line.split(':')
    user = user[1].split('!')[0]

    msg = line.split(':')[-1]

    if user == owner or ops.include? user
      #if it's me, we respond to commands, lol
      if /\bfoobot/ =~ msg
        if /\bhi/ =~ msg
          print_to_socket socket, 'PRIVMSG #foobar :Hello there.'
        end

        if /\badd/ =~ msg and /\bops/ =~ msg
          users = get_users socket

          msgparts = msg.split ' '
          added = false
          msgparts.each do |part|
            part = part.gsub /[,]*/, ''
            if users.include? part and part != 'foobot'
              ops << part
              print_to_socket socket, 'PRIVMSG #foobar :Added ' + part + ' to ops.'
              added = true
            end
          end

          if !added 
            print_to_socket socket, 'PRIVMSG #foobar :No user by that name :('
          end
        end

        if /\blist/ =~ msg and /\bops/ =~ msg
          print_to_socket socket, 'PRIVMSG #foobar :Ops: ' + ops.join(' ')
        end

        if /\bremove/ =~ msg and /\bops/ =~ msg
          msgparts = msg.split ' '
          removed = false
          msgparts.each do |part|
            part = part.gsub /[,]*/, ''
            if ops.include? part and part != 'foobot'
              ops.delete_at ops.find_index(part)
              print_to_socket socket, 'PRIVMSG #foobar :Removed ' + part + ' from ops.'
              removed = true
            end
          end

          if !removed
            print_to_socket socket, 'PRIVMSG #foobar :Couldn\'t find an op by that name'
          end
        end

        if /\bwho/ =~ msg and /\bowns/ =~ msg
          print_to_socket socket, 'PRIVMSG #foobar :My owner is ' + owner
        end

        if /\bwhere/ =~ msg and /\blive/ =~ msg
          print_to_socket socket, 'PRIVMSG #foobar :I live at https://github.com/hashtagfoobar/foobot/blob/master/bot.rb'
        end

        if /\bversion/ =~ msg 
          print_to_socket socket, 'PRIVMSG #foobar :I am version 0.0.1'
        end

      end
    end

    if user == owner
      if /\bleave/ =~ msg and /\bfoobot/ =~ msg
        print_to_socket socket, 'PRIVMSG #foobar :Adios, boss.'
        leave = true
      end

      if /\brestart/ =~ msg and /\bfoobot/ =~ msg
        print_to_socket socket, 'PRIVMSG #foobar :brb, boss.'
        leave = true
        restart = true
      end
    end
  end 
end

print_to_socket socket, 'QUIT :later, nerds.'
socket.close

if restart
  exit 1
else
  exit
end