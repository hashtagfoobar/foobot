Gem::Specification.new do |s|
  s.name        = 'IRC Bot - Foobot.'
  s.version     = '0.0.1'
  s.summary     = "Does irc bot stuff."
  s.description = "it's an irc bot that does things."
  s.authors     = ["Anthony Clever"]
  s.email       = 'anthonyclever@gmail.com'
  s.files       = `git ls-files`.split "\n"
  s.executables = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ['lib']
  s.homepage    = 'http://toribash-dev.leel.me/'
end