until ruby ../lib/bot.rb; do
    echo "Server bot crashed with exit code $?.  Respawning.." >&2
    sleep 1
done